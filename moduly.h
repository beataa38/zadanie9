struct Client
{
	char surname[25];
	struct Client *next;
};

int list_size(struct Client *head);
void show_list(struct Client *head);
void push_front(struct Client **head, char *p_surname); 
void push_back(struct Client **head, char *p_surname);
void push_by_index(struct Client **head, char *p_surname, int p_index);
void pop_front(struct Client **head); 
void pop_by_index(struct Client **head, int p_index);
void pop_by_surname(struct Client **head, char *p_surname);
void pop_back(struct Client **head);
