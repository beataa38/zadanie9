#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moduly.h"

int list_size(struct Client *head)
{
    int size = 0;
    if(head == NULL)
        return size;
    else
    {
        struct Client *somebody = head;
        // go through the whole list and count its elements, until you reach last element (which has next attribute on NULL)
        do
        {
            size++;
            somebody = somebody->next;
        } while (somebody != NULL);
    }

    return size;
}

void show_list(struct Client *head)
{
    if(head == NULL)
        printf("Empty list\n");
    else
    {
        printf("\nList of clients: \n");
        struct Client *somebody = head;
        do
        {
            printf("%s\n", somebody->surname);
            somebody = somebody->next;

        } while(somebody != NULL);
    }

    return;
}

// Add element to the beginning of a list (new head)
void push_front(struct Client **head, char *p_surname)
{
    struct Client *new;
    new = (struct Client*)malloc(sizeof(struct Client));

    strcpy(new->surname, p_surname);
    *head = new; // new element is a head now

    return;
}

// Add element to the end of a list (new tail)
void push_back(struct Client **head, char *p_surname)
{
    // list is empty, we create head
    if(*head == NULL)
    {
        *head = (struct Client*)malloc(sizeof(struct Client));
        strcpy((*head)->surname, p_surname);
        (*head)->next = NULL;
    }
    else // we create new element and go through whole list to add it at the end of list
    {
        struct Client *new = *head; // we create new list element and assign it to head

        // we set new elem to the last elem of list
        while(new->next != NULL)
        {
            new = new->next;
        }
        // allocate memory for new elem after the last one (new tail)
        new->next = (struct Client*)malloc(sizeof(struct Client));

        // assign values to the element after the last from the list (to new tail)
        strcpy(new->next->surname, p_surname);
        new->next->next = NULL;
    }

    return;
}

// Add element by index
void push_by_index(struct Client **head, char *p_surname, int p_index)
{
    if(p_index == 0)
        push_front(head, p_surname);
    else
    {
        if(p_index == list_size(*head))
            push_back(head, p_surname);
        else
        {
            struct Client *new = *head;
            struct Client *somebody; // auxiliary struct variable

            int i = 0;

            while(new->next != NULL && i<p_index-1)
            {
                new = new->next;
                i++;
            }

            somebody = new->next;
            new->next = (struct Client*)malloc(sizeof(struct Client));
            strcpy(new->next->surname, p_surname);
            new->next->next = somebody;
        }
    }

    return;
}

// Delete first element of a list (head)
void pop_front(struct Client **head)
{
    struct Client *somebody = NULL;

    if(*head != NULL) // head has to exist if we want to delete it
    {
        somebody = (*head)->next; // hold new head in somebody
        free(*head); // delete old head
        *head = somebody; // new head
    }

    return;
}

// Delete element by index
void pop_by_index(struct Client **head, int p_index)
{
	if(p_index == 0)
		pop_front(head);
	else
	{
		struct Client *somebody = *head;
		struct Client *somebody_else;

		int i = 0;
		while(somebody->next != NULL && i<p_index-1) // untill we reach end of the list or right index
		{
			somebody = somebody->next;
			i++;
		}
		somebody_else = somebody->next;
		somebody->next = somebody_else->next;
		free(somebody_else);
	}

	return;
}

// Delete first element of given name
void pop_by_surname(struct Client **head, char *p_surname)
{
    if(*head==NULL)
    {
        printf("Brak elementow do usuniecia, lista jest pusta.\n");
    }
    else
    {
        struct Client *new1 = *head;
        struct Client *pom = new1;

        while(new1->next != NULL)
        {
            if(strcmp(new1, p_surname)==0)
            {
                if(new1==*head)
                {
                    *head=new1->next;       //gdy usuwany element jest na poczatku listy
                    new1=pom->next;
                    free(pom);
                    break;
                }
                else
                {
                    pom->next = new1->next;         //gdy usuwany element jest w srodku listy
                    free(new1);
                    break;
                }

            }
            pom=new1;
            new1 = new1-> next;
        }

        if(new1->next == NULL && strcmp(new1, p_surname)==0)
        {
            pom->next = NULL;       //gdy usuwany element jest na koncu listy
            free(new1);
        }
    }
	return;
}

// Delete last element of a list (tail)
void pop_back(struct Client **head)
{
    if((*head)->next != NULL) // there is only head in this list
    {
        *head = NULL; // so delete this head
    }
    else // there are more elements
    {
        struct Client *somebody = *head;
        while(somebody->next->next != NULL) // till we reach the last element
        {
            somebody = somebody->next; // go to the next element
        }

        free(somebody->next); // delete last element
        somebody->next = NULL; // set pointer of new last elem to NULL
    }
    return;
}
