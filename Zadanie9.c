#include <stdio.h>
#include <stdlib.h>
#include "moduly.h"

int main()
{
	struct Client *head;
	head = (struct Client*)malloc(sizeof(struct Client));
	head = NULL;

	int ilosc = 0;
	int i = 0;
	char surname[25] = "";
	char decyzja = "";
	char decyzja2 = "";
	char decyzja3 = "";

	//zapytanie czy chce sie dodac klienta
	printf("Czy chcesz dodac nowych klientow? [t/n]: ");
	scanf(" %c", &decyzja);

	while (decyzja != 'n' && decyzja != 'N' && decyzja != 't' && decyzja != 'T')
    {
        printf("Niepoprawny znak! Podaj jeszcze raz! Czy chcesz dodac nowych klientow? [t/n]: ");
        scanf(" %c", &decyzja);
    }

	
    //dodanie klienta
    if (decyzja == 't' || decyzja == 'T')
    {
        printf("Podaj ilosc dodawanych klientow: ");
        scanf("%d", &ilosc);

        while (ilosc <= 0)
        {
            printf("Podalesc niewlasciwa ilosc. Podaj ponownie: ");
            scanf("%d", &ilosc);
        }

        for(i=0; i<ilosc; i++)
        {
            printf("Podaj nazwisko: ");
            scanf("%s", &surname);
            push_back(&head,surname);
        }

    }


    //usuwanie klienta
    printf("\nCzy chcesz usunac klienta? [t/n]: ");
    scanf(" %c", &decyzja2);
    while (decyzja2 != 'n' && decyzja2 != 'N' && decyzja2 != 't' && decyzja2 != 'T')
    {
        printf("Niepoprawny znak! Podaj jeszcze raz! Czy chcesz usunac klienta? [t/n]: ");
        scanf(" %c", &decyzja2);
    }
    if (decyzja2 == 't' || decyzja2 == 'T')
    {
        printf("Podaj nazwisko ktore chcesz usunac: ");
        scanf("%s", &surname);
        pop_by_surname(&head, surname);
		
		printf("Czy chcesz usunac kolejnego klienta? [t/n]: ");
		scanf(" %c", &decyzja3);
		while (decyzja3 != 'n' && decyzja3 != 'N' && decyzja3 != 't' && decyzja3 != 'T')
		{
			printf("Niepoprawny znak! Podaj jeszcze raz! Czy chcesz usunac kolejnego klienta? [t/n]: ");
			scanf(" %c", &decyzja3);
		}
		if (decyzja3 == 't' || decyzja3 == 'T')
		{
			printf("Podaj nazwisko ktore chcesz usunac: ");
			scanf("%s", &surname);
			pop_by_surname(&head, surname);
		}
		while (decyzja3 != 'n' && decyzja3 != 'N')
		{
			printf("Czy chcesz usunac kolejnego klienta? [t/n]: ");
			scanf(" %c", &decyzja3);
			while (decyzja3 != 'n' && decyzja3 != 'N' && decyzja3 != 't' && decyzja3 != 'T')
			{
				printf("Niepoprawny znak! Podaj jeszcze raz! Czy chcesz usunac kolejnego klienta? [t/n]: ");
				scanf(" %c", &decyzja3);
			}
			if (decyzja3 == 't' || decyzja3 == 'T')
			{
				printf("Podaj nazwisko ktore chcesz usunac: ");
				scanf("%s", &surname);
				pop_by_surname(&head, surname);
			}
		}
    }


	

    //wyswietlenie ilosci klientow
    printf("\nIlosc klientow: %d\n", list_size(head));

    //wyswietlenie nazwisk klientow
    show_list(head);
	
	printf("\n");
	system ("PAUSE");
	
	return 0;
}
